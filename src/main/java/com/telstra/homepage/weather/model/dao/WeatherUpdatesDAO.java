package com.telstra.homepage.weather.model.dao;

import org.springframework.stereotype.Component;

import com.telstra.homepage.weather.model.WeatherUpdates;

@Component
public class WeatherUpdatesDAO {

	public WeatherUpdates getWeatherUpdates(String zipCode) {
		WeatherUpdates weatherUpdates= new WeatherUpdates();
		
		if(zipCode.equalsIgnoreCase("500050")) {
			 weatherUpdates.setTemperature("40");
			 weatherUpdates.setHumidity("86");
			 return weatherUpdates;
	      }
		else if (zipCode.equalsIgnoreCase("500032")) {
			weatherUpdates.setTemperature("40");
			 weatherUpdates.setHumidity("84");
			 return weatherUpdates;
		}
		
		return weatherUpdates;
}
}
