/**
 * 
 */
package com.telstra.homepage.weather.model;

/**
 * @author Hemanth.Kunareddy
 *
 */
public class WeatherUpdates {

	private String temperature, humidity;

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getHumidity() {
		return humidity;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	
	
}
