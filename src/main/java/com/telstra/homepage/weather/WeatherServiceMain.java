/**
 * 
 */
package com.telstra.homepage.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;




/**
 * @author Hemanth.Kunareddy
 *
 */
@SpringBootApplication
public class WeatherServiceMain {

	/**
	 * @param args
	 */
	
	// TODO Auto-generated method stub
	public static void main(String[] args) {
		SpringApplication.run(WeatherServiceMain.class, args);
	}
			



}
