package com.telstra.homepage.weather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.homepage.weather.model.WeatherUpdates;
import com.telstra.homepage.weather.service.WeatherUpdatesService;

@RestController
@RequestMapping("/api/v1")
public class WeatherUpdatesController {

	@Autowired
	WeatherUpdatesService weatherUpdatesService;
	
	@GetMapping("/weather-updates/{zipCode}")
	public ResponseEntity<WeatherUpdates> getWeatherUpdates(@PathVariable("zipCode") String zipCode){
	    WeatherUpdates weatherUpdates =  weatherUpdatesService.getWeatherUpdates(zipCode);  	
		return new ResponseEntity<> (weatherUpdates, HttpStatus.OK); 
	}
	
}
