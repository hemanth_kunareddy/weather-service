/**
 * 
 */
package com.telstra.homepage.weather.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.homepage.weather.model.WeatherUpdates;
import com.telstra.homepage.weather.model.dao.WeatherUpdatesDAO;
import com.telstra.homepage.weather.service.WeatherUpdatesService;

/**
 * @author Hemanth.Kunareddy
 *
 */
@Service
public class WeatherUpdatesServiceImpl implements WeatherUpdatesService {

	@Autowired
	WeatherUpdatesDAO weatherUpdatesDAO;

	@Override
	public WeatherUpdates getWeatherUpdates(String zipCode) {
		 
		// TODO Auto-generated method stub
		WeatherUpdates weatherUpdates = weatherUpdatesDAO.getWeatherUpdates(zipCode);
		return weatherUpdates;
		
	}
	
	
	
	
}
