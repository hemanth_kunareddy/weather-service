package com.telstra.homepage.weather.service;



import com.telstra.homepage.weather.model.WeatherUpdates;


public interface WeatherUpdatesService {

	 public WeatherUpdates getWeatherUpdates(String zipCode);
}
